
Module: Google Analytics Injection
Author: Dmytro Novikov <https://drupal.org/user/478976>


Description
===========
The module injects any code (provided by another modules)
right after the _gaq.push(["_setAccount", "UA-XXXXXXX-X"]) call.

It can be used for adding any custom GA tags,
like _gaq.push(["_setPageGroup"...]);

The module does not provide any UI, it only fires the hook.


Requirements
============
* JS Alter module.
* Tracking code is added with drupal_add_js(..., 'inline', ...) call.
* Google Analytics module is optional since the condition above is true.


Usage
=====
Implement hook_google_analytics_inject() in your custom module. The function
should return any additional GA code, like _gaq.push(["_setPageGroup"...]);
